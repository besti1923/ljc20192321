public interface Multiplication {
    public Plurality multiplication(Plurality a, Plurality b);
}
