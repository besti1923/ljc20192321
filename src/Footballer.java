public class Footballer extends FCB implements Comparable{
    public Footballer(String name, int number) {
        super(name, number);
    }

    @Override
    public void pace(int a) {
        System.out.println("pace:"+a);
    }

    @Override
    public void shot(int b) {
        System.out.println("shot:"+b);
    }

    @Override
    public void dribble(int c) {
        System.out.println("dribble:"+c);
    }

    @Override
    public void ability(int d) {
        System.out.println("ability:"+d);
    }


    @Override
    public int compareTo(Object o) {
        FCB footballer = (FCB)o;
        if (this.number> footballer.getNumber())
        return 1;
        else {
            return 0;
        }
    }
}

