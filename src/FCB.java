public abstract class FCB {
    String name;
    int number;

    public abstract void pace(int a);
    public abstract void shot(int b);
    public abstract void dribble(int c);
    public abstract void ability(int d);

    public FCB(String name, int number) {
        this.name = name;
        this.number = number;
    }

    @Override
    public String toString() {
        return "FCB{" +
                "name='" + name + '\'' +
                ", number=" + number +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }
}
