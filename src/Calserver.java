import java.net.ServerSocket;
import java.io.*;
import java.net.Socket;
import java.util.Scanner;

public class Calserver {
    public static void main(String[] args) throws IOException {
        //建立服务器绑定窗口
        ServerSocket serverSocket =new ServerSocket(8809);
        //accept()方法处理连接请求，防止非法监听
        Socket socket =serverSocket.accept();
        //输入流
        InputStream inputStream=socket.getInputStream();
        BufferedReader bufferedReader=new BufferedReader(new InputStreamReader(inputStream));
        //输出流
        OutputStream outputStream =socket.getOutputStream();
        PrintWriter printWriter=new PrintWriter(outputStream);
        //读取用户信息
        String info=null;
        System.out.println("服务器正在建立...");
        //反馈信息
        while(!((info=bufferedReader.readLine())==null)){
            System.out.println("计算："+info);
            ComplexCall com =new ComplexCall(info);
            com.Cal();
            System.out.println(com.getResult());
            printWriter.write(com.getResult());
            printWriter.flush();
        }
        //关闭资源
        inputStream.close();
        outputStream.close();
        bufferedReader.close();
        printWriter.close();
        serverSocket.close();
        socket.close();

    }
}

