public class Football
{
        String name;
        String nation;
        String old;
        public String setName(String name )
        {
            this.name = name;
            return this.name;
        }
        public String setNation(String nation )
        {
            this.nation = nation;
            return this.nation;
        }
        public String setOld(String old )
        {
            this.old = old;
            return this.old;
        }
        public Football()
        {
            this.name = "";
            this.nation = "";
	    this.old = "";
	}
	public Football(String name,String nation,String old)
	{
	    this.name = name;
	    this.nation = nation;
	    this.old = old;
	}
	public void printFootball()
	{
	    System.out.print("name:"+this.name+"\n"+"nation:"+this.nation+"\n"+"old:"+this.old+"\n");
	}
}
