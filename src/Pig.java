public class Pig extends Animal implements Comparable{
    public Pig(String name, int id) {
        super(name, id);
    }

    @Override
    public void feed() {
        System.out.println("小猪：猪饲料");
    }

    @Override
    public void shark() {
        System.out.println("小猪：喽喽喽");
    }

    @Override
    public void move() {
        System.out.println("小猪：跑");
    }

    @Override
    public int compareTo(Object o) {
        return 0;
    }
}
