public class LinearNode {
    protected int element;

    protected LinearNode next = null;

    public LinearNode(int element) {
        this.element = element;
    }

    public int getElement() {
        return element;
    }

    public void setElement(int element) {
        this.element = element;
    }

    @Override
    public String toString() {
        return "LinearNode{" +
                "element=" + element +
                '}';
    }
}
