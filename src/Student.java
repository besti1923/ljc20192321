public class Student implements Comparable{
    protected String name;
    protected int number;
    protected String hobby;

    protected Student next = null;

    public Student(String name, int number, String hobby) {
        this.name = name;
        this.number = number;
        this.hobby = hobby;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", number=" + number +
                ", hobby='" + hobby + '\'' +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getHobby() {
        return hobby;
    }

    public void setHobby(String hobby) {
        this.hobby = hobby;
    }

    @Override
    public int compareTo(Object o) {
        return 0;
    }
}
