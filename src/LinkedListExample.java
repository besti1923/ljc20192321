public class LinkedListExample {
    public static void main(String[] args){
        Student hao = new Student("haoyixuan",20192321,"����Ϸ");
        Student bei = new Student("beishizhi",20192316,"����");
        Student guo = new Student("guoquanwu",20192325,"����");
        Student yang = new Student("yanglikai",20192326,"ѧϰ");

        Student niu = new Student("niuzimeng",20192328,"Learning Java");

        Student head = hao;
        hao.next = bei;
        bei.next = guo;
        guo.next = yang;

        /*niu.next = head;
        head = niu;*/

        /*Student point = head;
        while (point.next != null){
            point = point.next;
        }
        point.next = niu;*/

        PrintLinkedList(head);
    }

    public static void PrintLinkedList(Student Head){
        Student point = Head;
        while (point != null){
            System.out.println("name:"+point.name+" number:"+point.number+" hobby:"+point.hobby);
            point = point.next;
        }
    }
}
