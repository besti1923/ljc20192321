public interface Division {
    public Plurality division(Plurality a, Plurality b);
}
