import java.io.*;
import java.net.Socket;

public class CalCleint {
    public static void main(String[] args) throws IOException{
        //建立客户端
        Socket socket = new Socket("192.168.3.15",8800);
        //输入
        InputStream inputStream = socket.getInputStream();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream,"UTF-8"));
        //输出
        OutputStream outputStream = socket.getOutputStream();
        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream);
        //传递信息
        String infol="计算:1/4+1/6";
        outputStreamWriter.write(infol);
        outputStreamWriter.flush();
        socket.shutdownOutput();
        //接收响应
        String reply=null;
        while(!((reply =bufferedReader.readLine())==null)){
            System.out.println(reply);
        }
        //关闭资源
        bufferedReader.close();
        inputStream.close();
        outputStreamWriter.close();
        outputStream.close();
        socket.close();
    }
}