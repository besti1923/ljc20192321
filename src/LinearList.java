public class LinearList {
    public static void main(String[] args) {
        LinearNode node1 = new LinearNode(20);
        LinearNode node2 = new LinearNode(19);
        LinearNode node3 = new LinearNode(23);
        LinearNode node4 = new LinearNode(21);
        LinearNode node5 = new LinearNode(20);
        LinearNode node6 = new LinearNode(20);
        LinearNode node7 = new LinearNode(11);
        LinearNode node8 = new LinearNode(5);
        LinearNode node9 = new LinearNode(16);
        LinearNode node10 = new LinearNode(39);
        LinearNode node11 = new LinearNode(57);

        LinearNode head = node1;
        node1.next = node2;
        node2.next = node3;
        node3.next = node4;
        node4.next = node5;
        node5.next = node6;
        node6.next = node7;
        node7.next = node8;
        node8.next = node9;
        node9.next = node10;
        node10.next = node11;
        PrintLinkedList(head);
    }

    public static void PrintLinkedList(LinearNode Head){
        LinearNode node = Head;
        int nLijincheng = 0;
        while (node != null){
            nLijincheng++;
            System.out.println(node.element);
            node = node.next;
        }
        System.out.println(nLijincheng);
    }
}
