public abstract class Animal {
    String name;
    int id;

    public void feed(){
        System.out.println("投喂食物！");
    }

    public void shark(){
        System.out.println("动物叫声！");
    }

    public abstract void move();

    public Animal(String name, int id) {
        this.name = name;
        this.id = id;
    }

    @Override
    public String toString() {
        return "Animal{" +
                "name='" + name + '\'' +
                ", id=" + id +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
