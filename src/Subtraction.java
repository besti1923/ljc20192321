public interface Subtraction {
    public Plurality subtraction(Plurality a, Plurality b);
}
