import java.io.*;

public class FileTest {
    public static void main(String[] args) throws IOException {
        File file=new File("C:\\Users\\34983\\OneDrive\\文档\\学习\\Java","ljc.txt");
        file.createNewFile();
        System.out.println(file.exists());

        OutputStream outputStream=new FileOutputStream(file);
        byte[] hello={'H','e','l','l','o','l','j','c'};
        outputStream.write(hello);
        InputStream inputStream=new FileInputStream(file);
        while (inputStream.available()>0){
            System.out.print((char) inputStream.read()+"");
        }
        outputStream.close();
        inputStream.close();

        Writer writer=new FileWriter(file,true);
        writer.append("ljcnb");
        writer.flush();
        Reader reader=new FileReader(file);
        while (reader.ready()){
            System.out.print((char) reader.read()+"");
        }
    }
}